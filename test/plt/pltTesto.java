package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class pltTesto {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
		
	}
	@Test
	public void testTranslatotionEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL ,translator.translate());
		
	}
	
	@Test
	public void testTranslatotionPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseStartinWithAEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseStartingWithVowelEndingWithK() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseNotStartingWithVowelAndRemovingTheConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseStartingWithMoreConsonants() {
		String inputPhrase = "know";
		Translator translator = new Translator(inputPhrase);
		assertEquals("owknay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseIsOneWorldOrTwoWorldWhitSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseIsOneWorldOrTwoWorldWith() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay" ,translator.translate());
		
	}
	@Test
	public void testTranslatotionPhraseWithpuncta() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!" ,translator.translate());
		
	}

}
